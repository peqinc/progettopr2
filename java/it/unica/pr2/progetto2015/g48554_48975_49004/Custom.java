
package it.unica.pr2.progetto2015.g48554_48975_49004;

public class Custom {
    
    /**
     * Funzione di esecuzione.
     * Prende in ingresso due valori corrispondenti alle coordinate
     * di un luogo e le passa al costruttore della classe Meteo che contiene
     * l'unico metodo getData() che rende il vettore di info meteo.
     * @param args due stringhe corrispondenti a longitudine e latitudine
     * @return un vettore di 5 elementi contenente informazioni meteo
    */
    
    public Object execute(Object... args) {
        Meteo meteo = new Meteo((String) args[0],(String) args[1]);
        return meteo.getData();
    }

    /** 
     * @return un informazione di aiuto riguardante la funzione
    */
    
    public final String getHelp() {
        return "Rende informazioni meteo";
    } 
    
}
