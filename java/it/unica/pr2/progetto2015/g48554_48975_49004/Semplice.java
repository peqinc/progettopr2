
package it.unica.pr2.progetto2015.g48554_48975_49004;

public class Semplice implements it.unica.pr2.progetto2015.interfacce.SheetFunction{
   
    /** 
     * @param args valore intero
     * @return 1 se il numero è dispari, 0 altrimenti
    */
   
    public Object execute(Object... args) {
        if( (int)args[0] % 2 == 0 ) 
            return 0;
        else 
            return 1;
    }

    /** 
     * @return la categoria della funzione di LibreOffice
    */
    
    public final String getCategory() {
		return "Informazione";
	}

    /** 
     * @return un informazione di aiuto riguardante la funzione
    */
    
    public final String getHelp() {
		return "Rende 1 se il numero è dispari, 0 altrimenti";
	} 

    /**
     * @return il nome della funzione di LibreOffice
    */
    
    public final String getName() {
		return "VAL.DISPARI";
	}
    
}
