# README PROGETTO PR2 #

### Descrizione generale ###

Un progetto di integrazione di codice java in un foglio di calcolo Libreoffice.

##### Requisiti per il funzionamento: #####
* Ubuntu Linux 14.04
* jdk 1.7.0_55
* LibreOffice 4.4.3
* Obba 4.2.2 (non supporta jdk 1.8)

##### La cartella principale del repository contiene: #####
* JavaProject/ **--------------->**
_Contiene i file di progetto Netbeans_
* build/progetto2015.jar **--->**
_Contiene i compilati, unico file utilizzato nel foglio.ods per i test_
* java/**-------------------------->**
_Contiene i sorgenti java_
* libs/**--------------------------->**
_Contiene le librerie esterne utilizzate nel progetto_
* foglio.ods**-------------------->**
_Foglio di calcolo con le relative funzioni execute delle classi: **Semplice, Complessa, Custom**_

##### Altre classi/interfacce presenti nella cartella java/: #####
* Meteo.java (classe)**------------------>**
_Utilizza la libreria esterna di dvdme ([Repository](https://github.com/dvdme/forecastio-lib-java))
contenente varie funzioni semplificate delle API di forecast.io ([Forecast Developer](https://developer.forecast.io/))_
* TestClassi.java (classe)**------------->** 
_Classe "main", esegue tre test rispettivamente per le classi Semplice, Complessa e Custom_
* SheetFunction.java (interfaccia)**--->**
_Implementa una singola funzione del foglio elettronico_ 

##### Metodi execute: #####
* Semplice **------>**
_Prende in ingresso un numero, rende true(1) se è dispari e false(0) se è pari_
* Complessa **--->**
_Prende in ingresso due matrici, rende la somma delle somme dei quadrati dei valori delle due matrici_
* Custom **------->**
_Prende in ingresso due stringhe contenenti latitudine e longitudine e rende un vettore di stringe contenente data, ora, temperatura, pressione e umidità_

### Funzionamento ###

Per il corretto funzionamento devono necessariamente essere installate le corrette versioni menzionate precedentemente.
##### Passi: #####
1. Scaricare il repository
2. Estrarre il file zip 
3. Aprire il file foglio.ods che col codice Obba fa riferimento al file build/progetto2015.jar
4. **Per i test di TestClassi.java utilizzando Netbeans** aprire il progetto JavaProject da Netbeans

##### Obba: #####

Nel file foglio.ods sono presenti 3 fogli di calcolo rispettivamente per le classi Semplice, Complessa, Custom, in ognuno di essi vengono effettuate 4 chiamate alla funzione per dimostrare il corretto funzionamento. La nomenclatura è oggettoS/resultS per la Semplice, oggettoCo/resultCo per la Complessa, oggettoCu/resultCu per la Custom.

##### Documentazioni esterne: #####

* Obba link --> [obba.info](http://obba.info/)
* Funzioni Libreoffice calc --> [funzioni](https://help.libreoffice.org/Calc/Functions_by_Category/it)
* Libreria esterna utilizzata --> [libreria](https://github.com/dvdme/forecastio-lib-java)

### Info ###

###### Prorietari: ######
* Stefano Carta 48554
* Omar Desogus 48975
* Giovanni Bertulu 49004