
package it.unica.pr2.progetto2015.g48554_48975_49004;

public class TestClassi {

    public static void main(String args[]) {
        
        // TEST classe Semplice

        assert !(boolean)new Semplice().execute(12);
        assert (boolean)new Semplice().execute(23);
        assert !(boolean)new Semplice().execute(42);
        
        // TEST classe Complessa
        
        Integer[][] matrice1 = new Integer[3][3];
        Integer[][] matrice2 = new Integer[3][3];
        int result = 0;
        
        for(int i = 0; i < matrice1.length; i++)
            for(int j=0; j< matrice1[i].length; j++ ) {
                matrice1[i][j] = (int)(Math.random()*10);
                result += matrice1[i][j]*matrice1[i][j];
            }
        
        for(int i = 0; i < matrice2.length; i++)
            for(int j=0; j< matrice2[i].length; j++ ) {
                matrice2[i][j] = (int)(Math.random()*10);
                result += matrice2[i][j]*matrice2[i][j];
            }
        
        assert (int)new Complessa().execute(matrice1,matrice2) == result;
        
        // TEST classe Custom (solo con connessione internet)
        /*
        String[] string = new Meteo("30.12345","4.12345").getData();
        for(String element: string)
            System.out.print(element+"\n");
        */
    }
    
}
