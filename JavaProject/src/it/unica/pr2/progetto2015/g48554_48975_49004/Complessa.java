
package it.unica.pr2.progetto2015.g48554_48975_49004;

public class Complessa {
    
    /** 
     * @param args due matrici
     * @return la somma della somma dei quadrati dei valori delle due matrici
    */
    
    public Object execute(Object... args) {
        
        Integer[][] x = (Integer[][]) args[0];
        Integer[][] y = (Integer[][]) args[1];
        
        int sum = 0;
        
        for(int i=0; i< x.length; i++ ) 
            for(int j=0; j< x[i].length; j++ ) 
		sum += x[i][j]*x[i][j];
        
        for(int i=0; i< y.length; i++ ) 
            for(int j=0; j< y[i].length; j++ ) 
		sum += y[i][j]*y[i][j];
        
        return sum;
    }

    /** 
     * @return la categoria della funzione di LibreOffice
    */
    
    public final String getCategory() {
        return "Matrice";
    }

    /** 
     * @return un informazione di aiuto riguardante la funzione
    */
    
    public final String getHelp() {
        return "Calcola la somma della somma dei quadrati dei valori delle due matrici";
    } 

    /**
     * @return il nome della funzione di LibreOffice
    */     
    
    public final String getName() {
        return "SOMMA.SOMMA.Q";
    }
    
}
