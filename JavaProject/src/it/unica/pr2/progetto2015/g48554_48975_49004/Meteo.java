
package it.unica.pr2.progetto2015.g48554_48975_49004;

import com.github.dvdme.ForecastIOLib.FIOCurrently;
import com.github.dvdme.ForecastIOLib.ForecastIO;

/**
 * Meteo utilizza la libreria esterna ForecastIO-Lib-Java, questa
 * utilizza le API di forecast.io che fornisce il codice applicazione
 * sul sito apposito di developer indispensibile per l'utilizzo del codice.
 * link github libreria esterna --> https://github.com/dvdme/forecastio-lib-java
*/

public class Meteo {
    
    private String latitudine;
    private String longitudine;
    
    public Meteo(String latitudine, String longitudine){
        this.latitudine = latitudine;
        this.longitudine = longitudine;
    }
    
    /**
     * Rende dati meteo.
     * Crea una nuova classe di previsione meteo e tramite le coordinate
     * degli attributi elabora informazioni quali temperatura, pressione,
     * umidità, ora e data. Vi sono diverse altre informazioni non utilizzate
     * in questo contesto.
     * @return un vettore di 5 elementi contenente informazioni meteo
    */
    
    public String[] getData() {
    
        //Creazione della classe principale di previsione 
        // Prende in ingresso il codice della API dell'account
        // personale creato sul sito https://developer.forecast.io
        ForecastIO fio = new ForecastIO("b644a4a7ad66735abceecf9bf96e8e60");
        
        // Imposto latitudine e longitudine con apposito metodo
        fio.getForecast(latitudine, longitudine);
        
        // Classe di inizializzazione per la FIODataPoint, contenente 
        // informazioni utilizzare di seguito
        FIOCurrently currently = new FIOCurrently(fio);
        
        // Popolo il vettore info con 5 elementi poi visibili su LibreOffice
        String[] info = new String[5];
        // La chiave "time" rende un orario composto da data e ora unite,
        // in questo è ragionevole separarle con apposito metodo 
        // di manipolazione di sottostringhe ( substring() )
        info[0] = "Data:\n"+currently.get().getByKey("time").substring(0,10); 
        info[1] = "Ora:\n"+currently.get().getByKey("time").substring(11);
        info[2] = "Temperatura:\n"+currently.get().getByKey("temperature"); 
        info[3] = "Pressione:\n"+currently.get().getByKey("pressure");
        info[4] = "Umidità:\n"+currently.get().getByKey("humidity");
         
        return info;
        
    }
    
    
}
